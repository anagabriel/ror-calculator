# AQUA - Calculator
## 1. Purpose
This is the first project within AQUA’s onboarding process for software engineers. It will be a calculator web application written in Ruby on Rails that is to be deployed on Heroku. The calculator will be a basic four function calculator that also follows the order of operations. All of the source code will be stored on a repo through GitHub.

### 1.1 Requirements
* Build a simple calculator app
  * Multiplication, division, addition, subtraction
  * Parentheses
* Follow PEMDAS rules
* Commit your code to github
* Push your code to Heroku.com
* Ruby: 2.4.0
* Rails: 5.1.4

## 2. Design Outline
Since this is a web application, it will use a client-server model as its architecture and Ruby on Rails as its framework. I will, also, include service worker (for Chrome, Firefox, and Opera) that will sit between the cache and the client.

### 2.1 Components:
* Web Client - UI that displays a calculator that takes user input to produce an output; evaluation of the expression will happen on the server and respond with the answer
  * Service Worker - sits between the UI and the client’s cache to manage offline requests
  * HTTP Cache - filters and stores information retrieved from the server
* Server - receives requests from client to display the calculator, and sends the proper HTTP response containing the calculator

### 2.2 Diagram:
![diagram](images/diagram.png)

## 3. Design Issues

### 3.1 Functional Issues

#### Issue: How will user input be handled?
* __Option 1__: User will click buttons through the UI
* Option 2: User will type into an input box

I decided to force the user to use mouse clicks instead of key presses to enter in operands and operators.

#### Issue: When will evaluation occur?
* __Option 1__: User will press a button to evaluate the expression
* Option 2: Expression will be evaluated on each operator pressed

I decided to force the user to evaluate the expression using a single button as opposed to a running count, like a normal four function calculator.

#### Issue: How should history be handled?
* __Option 1__: A sidebar will show history that was saved to the cache
* Option 2: A sidebar will pull history from a database to populate history

I decided to use the cache to store this data, because I don’t really want users just yet for this application.  

### 3.2 Non-Functional Issues

#### Issue: What web application architecture will be used?
* __Option 1__: MVC
* Option 2: Flux

Since I am using Ruby on Rails, I’m going to use an MVC architecture when implementing this web application.

#### Issue: What web application framework will be used?
* __Option 1__: Ruby on Rails (Ruby 2.4.0 & Rails 5.1.4)
* Option 2: Node.js

Since this is supposed to written in Ruby on Rails, I am using Ruby on Rails as my web application framework. The Ruby version will be 2.4.0, and, for Rails, 5.1.4.

#### Issue: Where will the web application be deployed?
* __Option__ 1: Heroku
* Option 2: AWS
* Option 3: GCP

Since one of the requirement for the web application is to use Heroku, I’ve decided to go with Heroku as the location for deployment.

## 4. Design Details
### 4.1 Expression Evaluation Sequence Diagram
![evaluate](images/evaluate.png)

### 4.2 User Interface Mockups
![ui](images/ui.png)
