class CalculatorController < ApplicationController
  def index
  end

  def create
    @exp = eval(request.body.read)
    @sol = nil
    begin
      @sol = eval(@exp[:exp])
    rescue SyntaxError => se
      @sol = 'error'
    end

    rez = {'solution': @sol}
    render json: rez
  end
end
