Rails.application.routes.draw do
  root 'calculator#index'
  get 'calculator/index'
  match '/evaluate' => 'calculator#create', via: :post
  match '/evaluate' => 'calculator#create', via: :get
end
